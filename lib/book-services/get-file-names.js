import { readdir } from 'node:fs/promises'

export const getFileNames = async () => {
  const bookNames = []

  try {
    const files = await readdir('/home/grahf/make-nietzsche-db/lib/rawbooks')
    for (const file of files) {
      if (file === 'raw-book-template.js') continue
      bookNames.push(file)
    }
    return bookNames
  } catch (err) {
    console.error(err)
  }
}
