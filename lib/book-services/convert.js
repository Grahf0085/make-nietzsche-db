import { getFileNames } from './get-file-names.js'
import { formatText } from './format-text.js'
import { writeConvertedFile } from './write-converted-file.js'
import { acMenckenRaw } from '../rawbooks/acMencken.js'
import { bgeZimmernRaw } from '../rawbooks/bgeZimmern.js'
import { gmSamuelRaw } from '../rawbooks/gmSamuel.js'
import { h2hZimmernRaw } from '../rawbooks/h2hZimmern.js'
import { h2hHarveyRaw } from '../rawbooks/h2hHarvey.js'
import { acLudoviciRaw } from '../rawbooks/acLudovici.js'
import { gmJohnstonRaw } from '../rawbooks/gmJohnston.js'
import { tiLudoviciRaw } from '../rawbooks/tiLudovici.js'

//just make sure these are in alphbetical order
const rawBooks = [
  acLudoviciRaw,
  acMenckenRaw,
  bgeZimmernRaw,
  gmJohnstonRaw,
  gmSamuelRaw,
  h2hHarveyRaw,
  h2hZimmernRaw,
  tiLudoviciRaw,
]

const bookNames = await getFileNames()
bookNames.filter((item) => item === 'raw-book-template.js')

rawBooks.forEach((book, index) => {
  let infoContainer = {}
  let chapterContainer = []
  let paragraphContainer = []
  let footnotesContainer = []

  book.forEach((chapter) => {
    Object.entries(chapter).forEach(([key, value]) => {
      const formattedText = formatText(
        key,
        value,
        infoContainer,
        chapterContainer,
        paragraphContainer,
        footnotesContainer,
      )
      infoContainer = formattedText[0]
      chapterContainer = formattedText[1]
      paragraphContainer = formattedText[2]
      footnotesContainer = formattedText[3]
    })
  })
  writeConvertedFile(
    bookNames[index],
    infoContainer,
    chapterContainer,
    paragraphContainer,
    footnotesContainer,
  )
})
