//TODO consider removing paragraph numbers from actual text

import { selectText } from './select-text.js'
import { formatFootnotes } from './format-footnotes.js'

export const formatText = (
  key,
  bookSection,
  infoContainer,
  chapterContainer,
  paragraphContainer,
  footnotesContainer,
) => {
  if (key === 'bookInfo') {
    infoContainer.title = bookSection.title
    infoContainer.subTitle = bookSection.subTitle
    infoContainer.pubDate = bookSection.pubDate
    infoContainer.translator = bookSection.translator
    infoContainer.dateTranslated = bookSection.dateTranslated
  } else {
    chapterContainer.push(key)

    const paragraphNumbers =
      bookSection.match(/^\s+\d{1,3}A?\.([ ]|\n)/gm) || //A is for some paragraphs in BGE having a part A
      [] //some sections don't have numbers, ie poems

    const numberofParagraphs = paragraphNumbers.length || 1

    for (let i = 0; i < numberofParagraphs; i++) {
      const splitSections = {}
      splitSections.text = selectText(
        '',
        i,
        paragraphNumbers,
        numberofParagraphs,
        bookSection,
      )
        .trim()
        .replace(/(?<![a-z][ ]\d\d.)(?<![a-z][ ]\d.)(?<=\d\.)\n+/g, ' ')

      if (splitSections.text[0] === '0')
        splitSections.text = splitSections.text.substr(2)

      if (splitSections.text.match(/\[[0-9]+]/g))
        footnotesContainer.push(formatFootnotes(splitSections.text))

      footnotesContainer = [].concat(...footnotesContainer) //flatten out footnotesContainer

      splitSections.text = splitSections.text
        .replace(/[[0-9]+].+(.|\n)*/g, '')
        .trim()

      if (key.toUpperCase() === 'PREFACE' || key.toUpperCase() === 'PROLOGUE') {
        splitSections.chapter = 0
        splitSections.number = i + 1 + '.' //change to string because other paragraphs have letters //TODO change .paragraph to .number
      } else if (
        key.match(/\d+\.[ ][A-Z]+/g) ||
        (key
          .replace(/["]/g, '')
          .match(/^[A-Z]+[a-z]*[ ][A-Z]+[a-z]*[ ][A-Z]+[a-z]*([ ,]?)/gm) &&
          !key.match('THE HAMMER SPEAKETH')) ||
        key.match('THE ANTICHRIST') || //match chapter title
        key.match('Note') ||
        (key
          .replace('"', '')
          .replace(',', '')
          .match(/^([ ]|[A-Z])+\.([ ]|[A-Z])*/g) &&
          !key.match('AN EPODE.')) //replace quotes and commas in chapter titles
      ) {
        splitSections.chapter = chapterContainer.indexOf(key) //chapter number is index of chapter name in chapter container
        splitSections.number = paragraphNumbers[i].trim() //some paragraph numbers have letters
      } else if (key.match('THE HAMMER SPEAKETH')) {
        splitSections.chapter = 11
        splitSections.number = i + 1 + '.'
      } else if (key.match(/[a-zA-Z]/)) {
        splitSections.chapter = 10
        splitSections.number = i + 1 + '.' //change to string because other paragraphs have letters
      }
      paragraphContainer.push(splitSections)
    }
  }
  return [
    infoContainer,
    chapterContainer,
    paragraphContainer,
    footnotesContainer,
  ]
}
