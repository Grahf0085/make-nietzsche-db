import fs from 'fs'
const FileSystem = fs

export const writeConvertedFile = (
  bookTitle,
  infoContainer,
  chapterContainer,
  paragraphContainer,
  footnotesContainer
) => {
  const titleFormatRemoved = bookTitle.slice(0, -3)
  const bookSections = {
    info: JSON.stringify(infoContainer, null, 5),
    chapters: JSON.stringify([...new Set(chapterContainer)], null, 5),
    paragraphs: JSON.stringify(paragraphContainer, null, 5),
    footnotes: JSON.stringify(footnotesContainer, null, 5),
  }

  FileSystem.writeFileSync(
    `/home/grahf/make-nietzsche-db/lib/convertedBooks/${titleFormatRemoved}Converted.js`,
    '',
    (error) => {
      if (error) throw error
    }
  )

  Object.entries(bookSections).forEach(([sectionName, sectionContainers]) => {
    FileSystem.appendFileSync(
      `/home/grahf/make-nietzsche-db/lib/convertedBooks/${titleFormatRemoved}Converted.js`,
      `export const ${sectionName} = ` + sectionContainers + '\n',
      (error) => {
        if (error) throw error
      }
    )
  })
}
