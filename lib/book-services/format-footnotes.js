//TODO add textForNote to database

export const formatFootnotes = (text) => {
  const footnotesText = text.match(/\[[0-9].+((\n[^\\[]+)|.*)/gm)
  const returnArray = []
  const supNumbers = text.match(/\p{No}+/gu) //finds all superscripts in text to select text around the superscript in variable named textForNote

  footnotesText.forEach((note, index) => {
    const footnotesSections = {}
    const textForNote = text
      .substring(
        text.indexOf(supNumbers[index]) - 100,
        text.indexOf(supNumbers[index]) + 100,
      )
      .replace(/\[[0-9]+].+(.|\n)*/g, '') //don't want footnotes in text
      .trim()
    footnotesSections.text = textForNote //fix for cutting off words from +/- 100 chars
      .substring(
        textForNote.indexOf(' '),
        textForNote.length > 150
          ? textForNote.lastIndexOf(' ')
          : textForNote.length,
      )
      .trim()
    text.includes('J.M.K.') //catches multi line footnotes from H2H. Check if this can be removed now that regex changed
      ? (footnotesSections.note = text.substring(
          text.indexOf(note),
          text.indexOf('J.M.K.') + 6,
        ))
      : (footnotesSections.note = note.trim())
    returnArray.push(footnotesSections)
  })
  return returnArray
}
