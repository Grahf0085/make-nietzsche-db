export const selectText = (
  returnText,
  i,
  paragraphNumbers,
  numberofParagraphs,
  bookSection
) => {
  i === numberofParagraphs - 1
    ? (returnText = bookSection.substring(
        bookSection.indexOf(paragraphNumbers[i])
      )) //last paragraph of section
    : (returnText = bookSection.substring(
        bookSection.indexOf(paragraphNumbers[i]), //first to second to last paragraph of section
        bookSection.indexOf(paragraphNumbers[i + 1])
      ))
  return returnText
}
