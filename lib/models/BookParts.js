import pool from '../utils/pool.js'

export class BookParts {
  title
  subTitle
  pubDate
  translatedDate
  translatorName
  chapterNumber
  chapterName
  paragraphNumber
  paragraphText
  footnotes
  seeAlso

  constructor(row) {
    this.title = row.book_title
    this.subTitle = row.sub_title
    this.pubDate = row.pub_date
    this.translatedDate = row.translated_date
    this.translatorName = row.translator_name
    this.chapterNumber = row.chapter_number
    this.chapterName = row.chapter_name
    this.paragraphNumber = row.paragraph_number
    this.paragraphText = row.paragraph_text
    this.footnotes = row.footnote_text
    this.seeAlso = row.see_also
  }

  static async fullText(title) {
    // not used
    const { rows } = await pool.query(
      `
SELECT book_title, sub_title, pub_date, translated_date, translator_name, chapter_number, chapter_name, paragraph_number, paragraph_text
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
INNER JOIN chapters ON chapters.book_fk = books.id
INNER JOIN paragraphs ON paragraphs.chapter_fk = chapters.id
WHERE book_title = $1
`,
      [title]
    )
    return rows.map((row) => new BookParts(row))
  }

  static async allTitles() {
    const { rows } = await pool.query('SELECT book_title FROM books')
    return rows.map((row) => new BookParts(row))
  }

  static async translations(title) {
    const { rows } = await pool.query(
      `
SELECT translator_name
FROM translator
INNER JOIN books
ON books.translator_fk = translator.id
WHERE books.book_title = $1
`,
      [title]
    )
    return rows.map((row) => new BookParts(row))
  }

  static async bookInfo(title, translator) {
    const { rows } = await pool.query(
      `
SELECT book_title, sub_title, pub_date, translated_date, translator_name
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
WHERE book_title = $1 AND translator_name = $2
`,
      [title, translator]
    )
    return new BookParts(rows[0])
  }

  static async chapterInfo(title, translator) {
    const { rows } = await pool.query(
      `
SELECT chapter_number, chapter_name
FROM chapters
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE book_title = $1 and translator_name = $2
`,
      [title, translator]
    )
    return rows.map((row) => new BookParts(row))
  }

  static async chapterParagraphs(title, translator, chapter) {
    const { rows } = await pool.query(
      `
SELECT chapter_number, paragraph_number, paragraph_text, see_also
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
INNER JOIN chapters ON chapters.book_fk = books.id
INNER JOIN paragraphs ON paragraphs.chapter_fk = chapters.id
WHERE book_title = $1 AND translator_name = $2 AND chapter_number = $3
ORDER BY SUBSTRING(paragraph_number FROM '([0-9]+)')::BIGINT ASC, paragraph_number
`,
      [title, translator, chapter]
    )
    return rows.map((row) => new BookParts(row))
  }

  static async footnotes(title, translator, chapter, paragraph) {
    const { rows } = await pool.query(
      `
SELECT footnote_text
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
INNER JOIN chapters ON chapters.book_fk = books.id
INNER JOIN paragraphs ON paragraphs.chapter_fk = chapters.id
INNER JOIN footnotes ON footnotes.paragraph_fk = paragraphs.id 
WHERE book_title = $1 AND translator_name = $2 AND chapter_number = $3 AND paragraph_number = $4
`,
      [title, translator, chapter, paragraph]
    )
    return rows.map((row) => new BookParts(row))
  }

  static async seeAlso(paragraphFK) {
    const { rows } = await pool.query(
      `
SELECT translator_name, book_title, sub_title, chapter_number, chapter_name, paragraph_number, paragraph_text 
FROM paragraphs
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator on translator.id = books.translator_fk
WHERE paragraphs.id = $1
`,
      [paragraphFK]
    )
    return new BookParts(rows[0])
  }
}
