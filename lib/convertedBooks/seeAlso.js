export const seeAlso = [
  {
    firstBookTitle: 'Human, All Too Human',
    firstChapter: 2,
    firstParagraphNumber: '45',
    secondBookTitle: 'Beyond Good and Evil',
    secondChapter: 9,
    secondParagraphNumber: '260',
  },
  {
    firstBookTitle: 'The Geneology Of Morals',
    firstChapter: 1,
    firstParagraphNumber: '2',
    secondBookTitle: 'Beyond Good and Evil',
    secondChapter: 9,
    secondParagraphNumber: '257',
  },
]
