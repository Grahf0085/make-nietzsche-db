import { Router } from 'express'
import { BookParts } from '../models/BookParts.js'

export default Router()
  .get('/full-text/:title', async (req, res) => {
    // not used
    try {
      const text = await BookParts.fullText(req.params.title)
      res.send(text)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })

  .get('/titles-all', async (req, res) => {
    try {
      const titles = await BookParts.allTitles()
      res.send(titles)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })

  .get('/translations/:title', async (req, res) => {
    try {
      const text = await BookParts.translations(req.params.title)
      res.send(text)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })

  .get('/book-info/:title/:translator', async (req, res) => {
    try {
      const text = await BookParts.bookInfo(
        req.params.title,
        req.params.translator
      )
      res.send(text)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })

  .get('/chapter-info/:title/:translator', async (req, res) => {
    try {
      const text = await BookParts.chapterInfo(
        req.params.title,
        req.params.translator
      )
      res.send(text)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })

  .get('/paragraphs-info/:title/:translator/:chapter', async (req, res) => {
    try {
      const text = await BookParts.chapterParagraphs(
        req.params.title,
        req.params.translator,
        req.params.chapter
      )
      res.send(text)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })

  .get(
    '/footnotes/:title/:translator/:chapter/:paragraph',
    async (req, res) => {
      try {
        const text = await BookParts.footnotes(
          req.params.title,
          req.params.translator,
          req.params.chapter,
          req.params.paragraph
        )
        res.send(text)
      } catch (err) {
        res.status(500).send({ error: err.message })
      }
    }
  )

  .get('/see-also/:paragraphFK', async (req, res) => {
    try {
      const text = await BookParts.seeAlso(req.params.paragraphFK)
      res.send(text)
    } catch (err) {
      res.status(500).send({ error: err.message })
    }
  })
