import client from '../lib/utils/client.js'

const resetTables = async () => {
  try {
    await client.query(`
DROP TABLE IF EXISTS translator, books, chapters, paragraphs, footnotes, see_also;

CREATE TABLE translator (
   id SMALLSERIAL PRIMARY KEY NOT NULL,
   translator_name varchar NOT NULL
 );

CREATE TABLE books (
  id SMALLSERIAL PRIMARY KEY NOT NULL,
  book_title varchar NOT NULL,
  sub_title varchar,
  pub_date smallint NOT NULL,
  translated_date smallint NOT NULL,
  translator_fk smallint REFERENCES translator(id) NOT NULL
);

CREATE TABLE chapters (
  id SMALLSERIAL PRIMARY KEY NOT NULL,
  chapter_number smallint NOT NULL,
  chapter_name varchar NOT NULL,
  book_fk smallint REFERENCES books(id) NOT NULL
);

CREATE TABLE paragraphs (
  id SMALLSERIAL PRIMARY KEY NOT NULL,
  paragraph_number varchar NOT NULL,
  paragraph_text text NOT NULL,
  paragraph_tokens TSVECTOR NOT NULL,
  chapter_fk smallint REFERENCES chapters(id) NOT NULL
);

CREATE TABLE footnotes (
  id SMALLSERIAL PRIMARY KEY NOT NULL,
  footnote_text varchar UNIQUE NOT NULL,
  paragraph_fk smallint REFERENCES paragraphs(id) NOT NULL
);

CREATE TABLE see_also (
    id SMALLSERIAL PRIMARY KEY NOT NULL,
		paragraph_one_fk smallint NOT NULL REFERENCES paragraphs(id),
  	paragraph_two_fk smallint NOT NULL REFERENCES paragraphs(id)
);

INSERT INTO translator(translator_name)
VALUES
('Helen Zimmern'),
('H. L. Mencken'),
('Horace B. Samuel'),
('Alexander Harvey'),
('Anthony M. Ludovici'),
('Ian Johnston')
`)
  } catch (err) {
    console.log(err)
  } finally {
    client.end()
  }
}

resetTables()
