// help with wrapping .map in Promise.all from https://github.com/mduleone

import client from '../lib/utils/client.js'
import * as acLudovici from '../lib/convertedBooks/acLudoviciConverted.js'
import * as acMencken from '../lib/convertedBooks/acMenckenConverted.js'
import * as bgeZimmern from '../lib/convertedBooks/bgeZimmernConverted.js'
import * as gmSamuel from '../lib/convertedBooks/gmSamuelConverted.js'
import * as h2hZimmern from '../lib/convertedBooks/h2hZimmernConverted.js'
import * as h2hHarvey from '../lib/convertedBooks/h2hHarveyConverted.js'
import * as gmJohnston from '../lib/convertedBooks/gmJohnstonConverted.js'
import * as tiLudovici from '../lib/convertedBooks/tiLudoviciConverted.js'
import { seeAlso } from '../lib/convertedBooks/seeAlso.js'

const booksArray = [
  acLudovici,
  acMencken,
  bgeZimmern,
  gmJohnston,
  gmSamuel,
  h2hHarvey,
  h2hZimmern,
  tiLudovici,
]

seed()

function seed() {
  Promise.all(
    booksArray.map(async (book) => {
      // Add the book
      // **NOTE:** _MUST_ add the translator first, these queries each reference a translator and therefore require the translator exists
      await client
        .query(
          `
        INSERT INTO books
        VALUES (DEFAULT, $1, $2, $3, $4, (SELECT id FROM translator WHERE translator.translator_name = $5))
      `,
          [
            book.info.title,
            book.info.subTitle,
            book.info.pubDate,
            book.info.dateTranslated,
            book.info.translator,
          ],
        )
        .catch((err) => {
          const error = new Error('Book Error')
          error.book = book
          error.originalError = err

          throw error
        })

      // Add the chapters
      // **NOTE:** _MUST_ add the translator and book first, these queries each reference a book and therefore require the book exists
      await Promise.all(
        book.chapters.map((chapterName, chapterNumber) =>
          client.query(
            `
        INSERT INTO chapters
        VALUES (DEFAULT, $1, $2, (SELECT id FROM books WHERE books.book_title = $3 AND books.translator_fk = (SELECT translator_fk FROM books INNER JOIN translator ON translator.id = books.translator_fk WHERE translator_name = $4 LIMIT 1)))
      `,
            [chapterNumber, chapterName, book.info.title, book.info.translator],
          ),
        ),
      ).catch((err) => {
        const error = new Error('Chapter Error')
        error.book = book
        error.originalError = err

        throw error
      })

      // Add the paragraphs
      // **NOTE:** _MUST_ add the translator, book and chapters first, these queries each reference a book and a chapter, and therefore require the books and chapters exist
      await Promise.all(
        book.paragraphs.map((paragraph) =>
          client.query(
            `
        INSERT INTO paragraphs
        VALUES (DEFAULT, $1, $2, to_tsvector($2), (SELECT id FROM chapters WHERE chapters.chapter_number = $3 AND chapters.book_fk = (SELECT book_fk FROM chapters INNER JOIN books ON books.id = chapters.book_fk INNER JOIN translator ON translator.id = books.translator_fk WHERE book_title = $4 AND translator_name = $5 LIMIT 1)))
      `,
            [
              paragraph.number.replace('.', ''),
              paragraph.text,
              paragraph.chapter,
              book.info.title,
              book.info.translator,
            ],
          ),
        ),
      ).catch((err) => {
        const error = new Error('Paragraph Error')
        error.book = book
        error.originalError = err

        throw error
      })

      // Add the footnotes
      // **NOTE:** _MUST_ add the translator, book, chapters, and paragraphs first, these queries each reference a paragraph, and therefore require the paragraphs to exist
      await Promise.all(
        book.footnotes.map((footnote) =>
          client.query(
            `
        INSERT INTO footnotes
        VALUES (DEFAULT, $1, (SELECT id FROM paragraphs WHERE paragraph_text LIKE $2))
      `,
            [footnote.note, `%${footnote.text}%`],
          ),
        ),
      ).catch((err) => {
        const error = new Error('Footnote Error')
        error.book = book
        error.originalError = err

        throw error
      })
    }),
  )
    .catch((err) => {
      console.log(err)
    })
    .then(async () => {
      // Add the see also
      // **NOTE:** _MUST_ add the translator, book, chapters, and paragraphs first, these queries each reference a paragraph, and therefore require the paragraphs to exist
      await Promise.all(
        seeAlso.map((paragraph) =>
          client.query(
            `
INSERT INTO see_also (paragraph_one_fk, paragraph_two_fk)
SELECT subquery1.id AS paragraph_one_fk, subquery2.id AS paragraph_two_fk
FROM (
    SELECT paragraphs.id
    FROM paragraphs
    INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
    INNER JOIN books ON books.id = chapters.book_fk
    WHERE book_title = $1 AND chapter_number = $2 AND paragraph_number = $3
) AS subquery1
CROSS JOIN (
    SELECT paragraphs.id
    FROM paragraphs
    INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
    INNER JOIN books ON books.id = chapters.book_fk
    WHERE book_title = $4 AND chapter_number = $5 AND paragraph_number = $6
) AS subquery2;
`,
            [
              paragraph.firstBookTitle,
              paragraph.firstChapter,
              paragraph.firstParagraphNumber,
              paragraph.secondBookTitle,
              paragraph.secondChapter,
              paragraph.secondParagraphNumber,
            ],
          ),
        ),
      ).catch((err) => {
        const error = new Error('First See Also Error')
        error.originalError = err

        throw error
      })

      // Add the see also
      // **NOTE:** _MUST_ add the translator, book, chapters, and paragraphs first, these queries each reference a paragraph, and therefore require the paragraphs to exist
      await Promise.all(
        seeAlso.map((paragraph) =>
          client.query(
            `
INSERT INTO see_also (paragraph_one_fk, paragraph_two_fk)
SELECT subquery1.id AS paragraph_one_fk, subquery2.id AS paragraph_two_fk
FROM (
    SELECT paragraphs.id
    FROM paragraphs
    INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
    INNER JOIN books ON books.id = chapters.book_fk
    WHERE book_title = $4 AND chapter_number = $5 AND paragraph_number = $6
) AS subquery1
CROSS JOIN (
    SELECT paragraphs.id
    FROM paragraphs
    INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
    INNER JOIN books ON books.id = chapters.book_fk
    WHERE book_title = $1 AND chapter_number = $2 AND paragraph_number = $3
) AS subquery2;
`,
            [
              paragraph.firstBookTitle,
              paragraph.firstChapter,
              paragraph.firstParagraphNumber,
              paragraph.secondBookTitle,
              paragraph.secondChapter,
              paragraph.secondParagraphNumber,
            ],
          ),
        ),
      ).catch((err) => {
        const error = new Error('Second See Also Error')
        error.originalError = err

        throw error
      })
    })
    .catch((err) => {
      console.log(err)
    })
    .finally(() => {
      client.end()
    })
}
