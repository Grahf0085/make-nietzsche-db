import express from 'express'
import cors from 'cors'

import { error } from './lib/middleware/error.js'
import { notFound } from './lib/middleware/not-found.js'
// import { logger } from './lib/middleware/logger.js'

import bookPartsController from './lib/controllers/bookParts.js'

const app = express()

app.use(cors())
app.use(express.json())

// app.use(logger)

app.get('/', (req, res) => {
  //add /api after the /?
  res.send('No Books Here')
})

app.use('/api', bookPartsController)

app.listen(1844)

app.use(notFound)
app.use(error)

export default app
